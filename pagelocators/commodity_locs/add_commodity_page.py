# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By


class AddCommodityPage:
    """一级模块——添加商品模块"""
    #商品模块元素
    commodity_loc = (By.XPATH, '//*[@id="app"]/div/div[1]/div/ul/div/li[1]/div')
    #添加商品列表元素
    commodity_list_loc = (By.XPATH, '//*[@id="app"]/div/div[1]/div/ul/div/li[1]/ul/a[2]/li')
    """------------------筛选搜索------------------------"""
    #商品分类元素
    commodity_class_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div/div[2]/form/div[1]/div/span/span')
    #一级商品分类-服装元素
    one_clothing_class_loc = (By.XPATH, '//li[text()="服装"]')
    #二级商品分类-T恤元素
    two_Tshirt_class_loc = (By.XPATH, '//li[text()="T恤"]')
    #商品名称元素
    commodity_name_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div/div[2]/form/div[2]/div/div[1]/input')
    #副标题元素
    subtitle_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div/div[2]/form/div[3]/div/div[1]/input')
    #商品品牌元素
    commodity_brand_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div/div[2]/form/div[4]/div/div/div/input')
    #商品品牌苹果品牌元素
    Apple_brand_loc = (By.XPATH, '//span[text()="苹果"]')
    #下一步填写商品促销元素
    next_promotion_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div/div[2]/form/div[13]/div/button')
    #下一步填写商品属性元素
    next_properties_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div/div[3]/form/div[17]/div/button[2]/span')
    #下一步选择商品关联元素
    next_association_loc = (By.XPATH, '//span[text()="下一步，选择商品关联"]')
    #完成提交商品元素
    submit_complete_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div/div[5]/form/div[3]/div/button[2]')
    #完成提交商品-点击确定元素
    submit_yes_loc = (By.XPATH, '/html/body/div[4]/div/div[3]/button[2]/span')

    """------------------数据列表------------------------"""
    #数据列表的上下架按钮元素
    data_updownshelf_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[3]/div/div[3]/table/tbody/tr/td[6]/div/p[1]/div/span')

    """断言元素"""
    #商品名称和商品货号断言元素
    enter_searchassert_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[3]/div/div[3]/table/tbody/tr/td[4]/div/p[1]')
    # #商品品牌断言元素
    # enter_searchassert_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[3]/div/div[3]/table/tbody/tr/td[4]/div/p[1]')
    #商品上下架断言元素
    down_shelfassert_loc = (By.XPATH, '//div[class="el-switch is-checked" and aria-checked="true"]')
    """------------------数据列表------------------------"""
