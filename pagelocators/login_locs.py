# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By

from common.read_yaml import ReadYaml
data = ReadYaml("data.yml").get_yaml_data()#读取数据

class LoginLocator:
    """登录模块"""
    #账户
    username = (By.XPATH, '//*[@id="app"]/div/div[1]/div/form/div[2]/div/div/input')
    #密码
    password = (By.XPATH, '//*[@id="app"]/div/div[1]/div/form/div[3]/div/div/input')
    #登录按钮
    login_button = (By.XPATH, '//*[@id="app"]/div/div[1]/div/form/div[4]/div/button[1]')

    # @staticmethod
    # def order_kind(title):
    #     kind = (By.XPATH, '//*[@title="%s"]' % title)
    #     return kind
    #
    # @staticmethod
    # def order_operation(text):
    #     operation = (By.XPATH, '//li[text()="%s"]' % text)
    #     return operation
    #
    # @staticmethod
    # def operator_btn(text):
    #     operator = (By.XPATH, '//*[@id="orderOperatorBtns"]//*[text()="%s"]' % text)
    #     return operator
